from cv2 import cv2
import numpy as np

def nothing(x):
    pass
    
def create_color_range_window(name):
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(name,200,200)
    cv2.createTrackbar('H1',name,81,255,nothing)
    cv2.createTrackbar('S1',name,96,255,nothing)
    cv2.createTrackbar('V1',name,60,255,nothing)
    cv2.createTrackbar('H2',name,104,255,nothing)
    cv2.createTrackbar('S2',name,255,255,nothing)
    cv2.createTrackbar('V2',name,255,255,nothing)

#https://stackoverflow.com/questions/10948589/choosing-the-correct-upper-and-lower-hsv-boundaries-for-color-detection-withcv/48367205#48367205
#https://nbviewer.jupyter.org/github/jrobchin/Computer-Vision-Basics-with-Python-Keras-and-OpenCV/blob/master/notebook.ipynb
"""This will convert image to hsv and then apply the range"""
def seg_color_range(image, range1, range2):
    img = cv2.GaussianBlur(image, (5,5), 0)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    kernel = np.ones((2,2),np.uint8)
    mask = cv2.inRange(img, range1, range2)
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
    mask = cv2.dilate(mask, kernel, iterations=2)

    return mask

def seg_color_range_test(image, range1, range2):
    kernel = np.ones((2,2),np.uint8)
    cv2.imwrite('original.png', image)
    img = cv2.GaussianBlur(image, (5,5), 0)
    cv2.imwrite('blur.png', img)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    cv2.imwrite('hsv.png', img)
    
    mask = cv2.inRange(img, range1, range2)
    cv2.imwrite('mask.png', mask)
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    cv2.imwrite('open.png', mask)
    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
    cv2.imwrite('close.png', mask)
    mask = cv2.dilate(mask, kernel, iterations=2)
    cv2.imwrite('dilate.png', mask)

    return mask

#https://stackoverflow.com/a/63462764/2630434
def get_largest_contour(mask, image):
    contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    area = None
    center_x = 0
    center_y = 0
    if len(contours) != 0:
        cv2.drawContours(image, contours, -1, 255, 3)
        c = max(contours, key = cv2.contourArea)
        x,y,w,h = cv2.boundingRect(c)
        found_area = [(x-5,y-5), (x+w+5,y+h+5)]
        cv2.rectangle(image,found_area[0],found_area[1],(0,255,0),2)
        area = mask[found_area[0][1]:found_area[1][1], found_area[0][0]:found_area[1][0]]
        
        center_x = x + w//2
        center_y = y + h//2

    return (area, image, (center_x,center_y))

def draw_color_range(image, range1, range2):
    cv2.rectangle(image, (range1[0], range1[1]), (range2[0], range2[1]), 0)
