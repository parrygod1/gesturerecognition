#from sklearn.model_selection import train_test_split
from keras.models import Sequential, load_model
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.layers import Dense, Flatten, Dropout, Activation
from keras.optimizers import Adam
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing.image import ImageDataGenerator, load_img, img_to_array
import tensorflow as tf
import numpy as np
import json
from cv2 import cv2
import os
import argparse
import datetime

IMG_SIZE = 150
MODEL_NAME = 'test'
USETB = 0 #flag to enable tensor board

classes = { #must be in order of folders (most likely alphabetical)
    '2': 0,
    '3': 1,
    '4': 2,
    'C' : 3,
    'L': 4,
    'palm' : 5
}

classes_inv = {v: k for k, v in classes.items()}

loaded_model = None

log_dir = None
tensorboard_callback = None

#https://github.com/jrobchin/Computer-Vision-Basics-with-Python-Keras-and-OpenCV/blob/master/notebook.ipynb
def init_cnn_model():
    model = Sequential()

    model.add(Conv2D(32, (3, 3), input_shape=(54, 54, 1)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    
    model.add(Conv2D(64, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    

    model.add(Flatten())
    model.add(Dense(128))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(len(classes.keys()))) #output neurons for category
    model.add(Activation('softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    
    return model

#https://github.com/jrobchin/Computer-Vision-Basics-with-Python-Keras-and-OpenCV/blob/master/notebook.ipynb
def init_gen_data():
    training_datagen = ImageDataGenerator(
    rotation_range=50,
    width_shift_range=0.1,
    height_shift_range=0.1,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True,
    fill_mode='nearest'
    )

    validation_datagen = ImageDataGenerator(zoom_range=0.2, rotation_range=10)
    batch_size = 16

    training_generator = training_datagen.flow_from_directory(
    'data/training',
    target_size=(54, 54),
    batch_size=batch_size,
    color_mode='grayscale'
    )

    validation_generator = validation_datagen.flow_from_directory(
    'data/validation',
    target_size=(54, 54),
    batch_size=batch_size,
    color_mode='grayscale'
    )

    return {
        "training" : training_generator,
        "validation" : validation_generator
    }


def train():
    model = init_cnn_model()
    generated_data = init_gen_data()
    if USETB:
        write_generated_images(data["training_generator"])

    #https://github.com/jrobchin/Computer-Vision-Basics-with-Python-Keras-and-OpenCV/blob/master/notebook.ipynb
    batch_size = 16
    hist = model.fit_generator(
        generator=generated_data['training'],
        steps_per_epoch = 2400 // batch_size, #no of total training data/batch
        epochs=12,
        verbose=1,
        validation_data=generated_data['validation'],
        validation_steps = 804 // batch_size, #no of total validation data/batch
        workers=8,
        callbacks = [tensorboard_callback] if USETB else []
    )
    model.save('model/' + MODEL_NAME + '.hdf5')
    json.dump(hist.history, open('model/hist/' + MODEL_NAME + 'hist.json', 'w'))

#https://www.tensorflow.org/tensorboard/image_summaries
def write_generated_images(data):
    global log_dir
    logdir = "logs/train_data/" + datetime.datetime.now().strftime("%Y-%m-%d-%H:%M:%S")
    file_writer = tf.summary.create_file_writer(logdir)

    with file_writer.as_default():
        # Don't forget to reshape.
        imgs = [data.__getitem__(i)[0] for i in range(0,25)]
        images = np.reshape(imgs, (-1, 54, 54, 1))
        tf.summary.image("25 training data examples", images, max_outputs=25, step=0)


def predict_gesture_test():
    loaded_model = load_model('model/' + MODEL_NAME)
    max_samples = 134
    count = {k: 0 for k, v in classes.items()}
    
    for c in classes:
        path = r'data/validation/' + c + '/'
        data_list = os.listdir(path)

        for i in range(1, max_samples):
            img = cv2.imread(path + data_list[i], cv2.IMREAD_GRAYSCALE)
            img = cv2.resize(img, (54, 54))
            arr = np.reshape(img, (1, 54, 54, 1)) #it has to be an array of (data_count, size_w, size_h, channel_count) where channel_count should be 1
            pred = loaded_model.predict(arr)
            #print(data_list[i], ' -> ', classes_inv[np.argmax(pred)])
            if c == classes_inv[np.argmax(pred)]:
                count[c] += 1
        count[c] = count[c] * 100 / max_samples
    print(count)

def init(model_name):
    global loaded_model
    loaded_model = load_model(model_name)

minimum_gest_score = 0.5

def predict_gesture(frame):
    img = cv2.resize(frame, (54, 54))
    #img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) 
    arr = np.reshape(img, (1, 54, 54, 1))
    pred = loaded_model.predict(arr)
    if np.max(pred) < minimum_gest_score:
        return 'none'
    return classes_inv[np.argmax(pred)]

if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser(prog='model.py', description='Execute the model',
                                        usage='%(prog)s action (\'train\'/\'test\') model_name usetb?(0/1)')
    arg_parser.add_argument('action', type=str)
    arg_parser.add_argument('model_name', type=str)
    arg_parser.add_argument('tb', type=int)
    args = arg_parser.parse_args()
    
    if len(args.model_name): 
        MODEL_NAME = args.model_name

    if args.tb == 1:
        USETB = 1
    
    if USETB:
        log_dir = 'testlogs'
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

    if args.action == 'train':
        train()
    elif args.action == 'test':
        predict_gesture_test()
    
   

