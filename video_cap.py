from cv2 import cv2
import argparse
import sys

arg_parser = argparse.ArgumentParser(prog='video_cap.py', description='Capture videos',
                                        usage='%(prog)s video_name max_frames do_capture(1|0). Press q to quit.')
arg_parser.add_argument('video_name', type=str, help='video name with path')
arg_parser.add_argument('max_frames', type=int)
arg_parser.add_argument('do_capture', type=int, nargs='?', help='default is 0, means just testing')
arg_parser.add_argument('test_vid', type=str, nargs='?')
args = arg_parser.parse_args()

folder = 'D:\Scoala\licenta\gesturerecognition\\videos\\'
video_name = ""
test_vid = ""
max_frames = 0
DO_CAPTURE = 0

if args.video_name:
    video_name = args.video_name
if args.test_vid:
    test_vid = args.test_vid
if args.max_frames:
    max_frames = args.max_frames
if args.do_capture:
    DO_CAPTURE = args.do_capture

codec = None 
out = None 

if DO_CAPTURE:
    codec = cv2.VideoWriter_fourcc(*'avc1') #(*'MP42')
    out = cv2.VideoWriter(folder + video_name, codec, 20.0, (640, 480))

if __name__ == '__main__':
    cap = None
    if len(test_vid):
        cap = cv2.VideoCapture(test_vid)
    else:
        cap = cv2.VideoCapture(0)

    frame_count = 0

    while cap.isOpened() and frame_count <= max_frames:
        ret, frame = cap.read()
        if ret:
            frame = cv2.resize(frame, (640, 480))

            if DO_CAPTURE:
                out.write(frame)

            cv2.putText(frame, str(frame_count), (30,70), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255))
            cv2.imshow('Video', frame)

            frame_count += 1
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        else:
            break

    cap.release()
    if out:
        out.release()
    cv2.destroyAllWindows()

