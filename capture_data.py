from cv2 import cv2
import numpy as np
import image_segmentation as imseg
import os
import argparse

arg_parser = argparse.ArgumentParser(prog='capture_data.py', description='Capture image data from videos and save to disk',
                                        usage='%(prog)s data_name video_capture_path max_frames do_capture(1|0). Press q to quit.')

arg_parser.add_argument('data_name', type=str, help='label for data')
arg_parser.add_argument('video_capture_path', type=str, help='video to extract data from')
arg_parser.add_argument('max_frames', type=int)
arg_parser.add_argument('do_capture', type=int, nargs='?', help='default is 0, means just testing')
args = arg_parser.parse_args()

filename = ''
folder = r'D:\\Scoala\\licenta\\gesturerecognition\\data\\'
video_cap = ''
DO_CAPTURE = 0
max_frames = 1400

if args.data_name:
    filename = args.data_name
if args.video_capture_path:
    video_cap = args.video_capture_path
if args.max_frames:
    max_frames = args.max_frames
if args.do_capture:
    DO_CAPTURE = args.do_capture

frame_count = 0
img_size = 150
imgs = []
DELAY = 1

def write_image(name, img):
    if DO_CAPTURE:
        if not cv2.imwrite(name, img):
            raise Exception("Could not write image")

def create_dirs(): 
    directory_train = folder + 'training\\' + filename
    directory_validation = folder+ 'validation\\' + filename
    os.mkdir(directory_train)
    os.mkdir(directory_validation)

if __name__ == '__main__':
    if len(video_cap):
        cap = cv2.VideoCapture(video_cap)
    else:
        cap = cv2.VideoCapture(0)

    if DO_CAPTURE:
        create_dirs()

    imseg.create_color_range_window('image')
    while frame_count <= max_frames:
        ret,img=cap.read()

        range1 = (cv2.getTrackbarPos('H1','image'), cv2.getTrackbarPos('S1','image'), cv2.getTrackbarPos('V1','image'))
        range2 = (cv2.getTrackbarPos('H2','image'), cv2.getTrackbarPos('S2','image'), cv2.getTrackbarPos('V2','image'))

        mask = imseg.seg_color_range(img, range1, range2)
        area, img, center = imseg.get_largest_contour(mask, img)

        cv2.imshow('Area', area)
        cv2.imshow('image', img)
        cv2.imshow('mask', mask)

        #imgs.append(area)
        newname = os.path.join(folder + 'training', filename, filename + '_' + str(frame_count) + '.png')

        if frame_count % 3 == 0 and frame_count % 7 > 0:
            newname = os.path.join(folder + 'training', filename, filename + '_' + str(frame_count) + '.png')
            write_image(newname, area)
        elif frame_count % 3 > 0 and frame_count % 7 == 0:
            newname = os.path.join(folder + 'validation', filename, filename + '_' + str(frame_count) + '.png')
            write_image(newname, area)


        frame_count += 1
        if(cv2.waitKey(DELAY) & 0xFF == ord('q')):
            break
