
from PyQt5 import QtGui
from PyQt5.QtWidgets import *
from rangeslider import QRangeSlider
from PyQt5.QtGui import *
import sys
from cv2 import cv2
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QThread, QObject
import numpy as np
import gestrecog
from functools import partial
from pynput.keyboard import Listener as KeyboardListener
from pynput.mouse import Listener as MouseListener
from gestinputs import CustomBindings
import gestserialize
import os
import subprocess

MainController = gestrecog.GestureRecognitionController()
Serializer = gestserialize.GestureRecognitionSearlize(MainController)

# https://gist.github.com/docPhil99/ca4da12c9d6f29b9cea137b617c7b8b1
class VideoThread(QThread):
    change_pixmap_signal = pyqtSignal(np.ndarray)

    def __init__(self, controller):
        super().__init__()
        self._run_flag = True
        self.controller = controller

    def run(self):
        #cap = cv2.VideoCapture('videos/modeltest.mp4')
        cap = cv2.VideoCapture(0)
        if cap.isOpened():
            self.controller.cap = cap
            self.controller.init_tracking_area()
        while self._run_flag:
            ret, frame = cap.read()
            if ret:
                timer = cv2.getTickCount()
                self.controller.update_frame(frame)
                self.controller.process(timer)
                self.change_pixmap_signal.emit(self.controller.get_frame())
            cv2.waitKey(15)
        # shut down capture system
        cap.release()

    def stop(self):
        """Sets run flag to False and waits for thread to finish"""
        self._run_flag = False
        self.wait()


def lol(n):
    print(n)


selected_gesture_binding = None
current_binding_type = None
can_hover_select = True


def set_selected_gesture_binding(new_type, gesture):
    global selected_gesture_binding
    global current_binding_type
    if can_hover_select:
        current_binding_type = new_type
        selected_gesture_binding = gesture
        print(selected_gesture_binding)

def get_bind_text(bind):
    if callable(bind):
        return bind.__name__
    else:
        return str(bind)

class GestureBindingsContainer:
    def __init__(self):
        self.gesture = ""
        self.Qtext = None
        self.button = None
        self.line = 0
        self.column = 0

    def update_hover(self):
        self.button.enterEvent = lambda e: set_selected_gesture_binding(
            "label", self.gesture)


class GestSeqBindingContainer:
    def __init__(self):
        self.gest_sequence = ()
        self.show_string = ""
        self.Qtext = None
        self.button = None
        self.line = 0
        self.column = 0

    def update_hover(self):
        self.button.enterEvent = lambda e: set_selected_gesture_binding(
            "sequence", self.gest_sequence)

    def get_sequence_string(self, sequence):  # sequence should be a tuple/list
        return ' '.join([l + ", " for l in sequence])


class GestureDirBindingsContainer:
    def __init__(self):
        self.gesture = ()
        self.Qtext = None
        self.show_string = ""
        self.button = None
        self.line = 0
        self.column = 0

    def update_hover(self):
        self.button.enterEvent = lambda e: set_selected_gesture_binding(
            "direction", self.gesture)

    def get_dir_string(self, sequence):  # sequence should be a tuple/list
        return ' '.join([l + ", " for l in sequence])


class SliderContainer:
    def __init__(self, index):
        self.slider = QRangeSlider()
        self.slider.setFixedWidth(200)
        self.slider.setMin(0)
        self.slider.setMax(255)
        self.slider.setMinimumHeight(20)
        self.slider.setMaximumHeight(20)
        self.label = "Slider"
        self.label_obj = None

        if index == 0:
            self.label = "Hue"
        elif index == 1:
            self.label = "Saturation"
        elif index == 2:
            self.label = "Value"

    def update_color_range(self, index):
        if index == 0:
            MainController.update_range_h(
                self.slider.start(), self.slider.end())
        elif index == 1:
            MainController.update_range_s(
                self.slider.start(), self.slider.end())
        elif index == 2:
            MainController.update_range_v(
                self.slider.start(), self.slider.end())


class KeyboardInputDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        # self.setWindowFlags(Qt.FramelessWindowHint)
        self.parent = parent
        self.mainlayout = QVBoxLayout()
        self.setLayout(self.mainlayout)
        self.mainlayout.addWidget(QLabel("Enter a keyboard key"))
        self.listener = None
        self.key = None
        self.hide()

    def showEvent(self, event):
        global can_hover_select
        can_hover_select = False

    def hideEvent(self, event):
        global can_hover_select
        can_hover_select = True

    def on_press(self, key):
        pass

    def on_release(self, key):
        # print(key)
        self.parent.update_gesture_binding(current_binding_type, key)
        self.hide()
        return False

    def wait_input(self):
        self.show()
        self.listener = KeyboardListener(on_release=self.on_release)
        self.listener.start()


class MouseInputDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        # self.setWindowFlags(Qt.FramelessWindowHint)
        self.parent = parent
        self.mainlayout = QVBoxLayout()
        self.setLayout(self.mainlayout)
        self.mainlayout.addWidget(QLabel("Press a mouse button"))
        self.listener = None
        self.key = None
        self.hide()

    def showEvent(self, event):
        global can_hover_select
        can_hover_select = False

    def hideEvent(self, event):
        global can_hover_select
        can_hover_select = True

    def on_scroll(self, x, y, dx, dy):
        if dy > 0:
            print("up")
            # self.parent.update_gesture_binding(current_binding_type, key)
        else:
            print("down")

        return False
        self.hide()

    def on_click(self, x, y, key, pressed):
        # print(key)
        self.parent.update_gesture_binding(current_binding_type, key)
        self.hide()
        return False

    def wait_input(self):
        self.show()
        self.listener = MouseListener(
            on_click=self.on_click, on_scroll=self.on_scroll)
        self.listener.start()


class GestureContainer:
    def __init__(self, parent, position):
        self.parent = parent
        self.position = position
        self.editable = False
        self.button = QPushButton('+')
        self.button.setMaximumWidth(35)
        self.menu, self.actions = self.create_gesture_menu()
        self.button.setMenu(self.menu)
        self.actions.setDisabled(True)
        self.gesture = ""

    def update_container(self, gesture):
        self.button.setText(gesture)
        if not self.editable:
            self.actions.setDisabled(False)
            self.editable = True
            self.parent.add_sequence(gesture)
            self.gesture = gesture
            self.parent.create_seq_button()
        else:
            self.parent.update_entry(gesture, self.position)

    def create_gesture_menu(self):
        menu = QMenu(self.parent)
        gestsubmenu = menu.addMenu("Gestures")
        for gesture in MainController.gesture_data.available_gestures:
            action = gestsubmenu.addAction(gesture)
            action.triggered.connect(partial(self.update_container, gesture))
        menu.addMenu(gestsubmenu)
        a1 = menu.addAction("<-")
        a1.triggered.connect(partial(self.parent.shift_entry_left, self))
        a2 = menu.addAction("->")
        a2.triggered.connect(partial(self.parent.shift_entry_right, self))
        a3 = menu.addAction("Delete")
        a3.triggered.connect(partial(self.parent.delete_entry, self))
        actions = QActionGroup(menu)
        actions.addAction(a1)
        actions.addAction(a2)
        actions.addAction(a3)
        return menu, actions


class SequenceCreatorDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        # self.setWindowFlags(Qt.FramelessWindowHint)
        self.resize(300, 200)
        self.parent = parent
        self.mainlayout = QVBoxLayout()
        self.setLayout(self.mainlayout)
        self.topbox = QHBoxLayout()
        self.bottombox = QHBoxLayout()
        self.mainlayout.addLayout(self.topbox)
        self.mainlayout.addLayout(self.bottombox)

        self.temp_sequence = []
        self.seq_buttons = []
        self.create_confirm_buttons()
        self.hide()

    def update_positions(self):
        i = 0
        new_seq = []
        for container in self.seq_buttons:
            container.position = i
            if container.gesture != "":
                new_seq.append(container.gesture)
            i += 1
        self.temp_sequence = new_seq

    def swap_positions(self, container, newpos):
        elem = self.seq_buttons[newpos]
        self.seq_buttons[container.position] = elem
        self.seq_buttons[newpos] = container
        self.topbox.removeWidget(container.button)
        self.topbox.insertWidget(newpos, container.button)
        self.update_positions()

    def shift_entry_left(self, container):
        if container.position > 0:
            newpos = max(0, (container.position - 1))
            self.swap_positions(container, newpos)
        print(self.temp_sequence)

    def shift_entry_right(self, container):
        if container.position < len(self.temp_sequence):
            newpos = min((container.position + 1), len(self.temp_sequence)-1)
            self.swap_positions(container, newpos)
        print(self.temp_sequence)

    def update_entry(self, gesture, position):
        self.temp_sequence[position] = gesture
        self.seq_buttons[position].gesture = gesture
        print(self.temp_sequence)

    def delete_entry(self, container):
        self.topbox.removeWidget(container.button)
        self.seq_buttons.remove(container)
        container.button.deleteLater()
        self.temp_sequence.pop(container.position)
        self.update_positions()

    def add_sequence(self, gesture):
        self.temp_sequence.append(gesture)
        print(self.temp_sequence)

    def create_confirm_buttons(self):
        confirm = QPushButton("Confirm")
        cancel = QPushButton("Cancel")
        confirm.clicked.connect(self.save_sequence)
        cancel.clicked.connect(self.cancel_sequence)
        self.bottombox.addWidget(confirm)
        self.bottombox.addWidget(cancel)

    def create_seq_button(self):
        buttonobj = GestureContainer(self, len(self.temp_sequence))
        self.topbox.addWidget(buttonobj.button)
        self.seq_buttons.append(buttonobj)

    def save_sequence(self):
        try:
            if len(self.temp_sequence) > 0:
                seq = MainController.gesture_sequencer.insert_full_sequence(
                    self.temp_sequence)
                mainapp.insert_sequence_binding(seq)
        except Exception as e:
            msg = QErrorMessage()
            msg.showMessage(str(e))
            msg.exec_()
        self.temp_sequence = []
        self.hide()
        self.clear_buttons()
        print(self.seq_buttons)

    def cancel_sequence(self):
        self.temp_sequence = []
        self.hide()
        self.clear_buttons()
        print(self.seq_buttons)

    def clear_buttons(self):
        for buttonobj in self.seq_buttons:
            self.topbox.removeWidget(buttonobj.button)
            buttonobj.button.deleteLater()
        self.seq_buttons.clear()

    def showEvent(self, event):
        self.create_seq_button()


class App(QWidget):
    testsignal = pyqtSignal()

    gestures_bindings_ui = {}
    sequence_bindings_ui = {}
    dir_bindings_ui = {}

    def update_gesture_binding(self, in_type, key):
        global selected_gesture_binding
        global current_binding_type

        keytext = ""
        if callable(key):
            keytext = key.__name__
        else:
            keytext = str(key)

        if current_binding_type == "label":
            MainController.gesture_bindings.bindings[selected_gesture_binding]['key'] = key
            self.gestures_bindings_ui[selected_gesture_binding].Qtext.setText(
                selected_gesture_binding + " : " + keytext)
        elif current_binding_type == "sequence":
            MainController.gesture_sequencer.bindings[selected_gesture_binding]['key'] = key
            container = self.sequence_bindings_ui[selected_gesture_binding]
            container.Qtext.setText(container.show_string + " : " + keytext)
        elif current_binding_type == "direction":
            MainController.gesture_dir_bindings.bindings[selected_gesture_binding]['key'] = key
            container = self.dir_bindings_ui[selected_gesture_binding]
            container.Qtext.setText(str(container.gesture) + " : " + keytext)

    def toggle_binding_hold(self):
        global selected_gesture_binding
        global current_binding_type

        if current_binding_type == 'label':
            MainController.gesture_bindings.bindings[selected_gesture_binding][
                'hold'] = not MainController.gesture_bindings.bindings[selected_gesture_binding]['hold']
        elif current_binding_type == 'direction':
            MainController.gesture_dir_bindings.bindings[selected_gesture_binding][
                'hold'] = not MainController.gesture_dir_bindings.bindings[selected_gesture_binding]['hold']

    def clear_binding(self):
        if current_binding_type == "label":
            MainController.gesture_bindings.bindings[selected_gesture_binding]['key'] = None
            self.gestures_bindings_ui[selected_gesture_binding].Qtext.setText(
                selected_gesture_binding + " : " + '')
        elif current_binding_type == "sequence":
            MainController.gesture_sequencer.bindings[selected_gesture_binding]['key'] = None
            container = self.sequence_bindings_ui[selected_gesture_binding]
            container.Qtext.setText(container.show_string + " : " + '')
        elif current_binding_type == "direction":
            MainController.gesture_dir_bindings.bindings[selected_gesture_binding]['key'] = None
            container = self.dir_bindings_ui[selected_gesture_binding]
            container.Qtext.setText(str(container.gesture) + " : " + '')

    def create_sliders(self):
        slider_arr = []
        for i in range(3):
            container = SliderContainer(i)
            container.slider.endValueChanged.connect(
                partial(container.update_color_range, i))
            container.slider.startValueChanged.connect(
                partial(container.update_color_range, i))
            slider_arr.append(container)

        return slider_arr

    def update_slider_values(self):
        for i in range(3):
            self.sliders[i].slider.setRange(
                int(MainController.range1[i]), int(MainController.range2[i]))

    def add_sliders(self):
        for slider_container in self.sliders:
            slider_container.label_obj = QLabel(slider_container.label)
            self.vlayout2.addWidget(slider_container.label_obj)
            self.vlayout2.addWidget(slider_container.slider)

    def add_checkboxes_settings(self):
        self.hand_checkbox = QCheckBox("Use Left Hand")
        self.hand_checkbox.stateChanged.connect(MainController.change_hand)
        self.vlayout2.addWidget(self.hand_checkbox)

        self.tracking_checkbox = QCheckBox("Track mouse (F1)")
        self.tracking_checkbox.stateChanged.connect(self.toggle_mouse_tracking)
        self.vlayout2.addWidget(self.tracking_checkbox)

        self.prediction_checkbox = QCheckBox("Gesture Prediction")
        self.prediction_checkbox.stateChanged.connect(
            self.toggle_gesture_prediction)
        self.vlayout2.addWidget(self.prediction_checkbox)

        self.binds_checkbox = QCheckBox("Gesture Binds")
        self.binds_checkbox.stateChanged.connect(self.toggle_gesture_binds)
        self.vlayout2.addWidget(self.binds_checkbox)

        self.sequence_checkbox = QCheckBox("Sequence Binds")
        self.sequence_checkbox.stateChanged.connect(self.toggle_sequence)
        self.vlayout2.addWidget(self.sequence_checkbox)

        self.direction_checkbox = QCheckBox("Direction Binds")
        self.direction_checkbox.stateChanged.connect(self.toggle_direction_binds)
        self.vlayout2.addWidget(self.direction_checkbox)

        layout = QHBoxLayout()
        self.radius_spinbox = QSpinBox()
        self.radius_spinbox.valueChanged.connect(self.update_reset_radius)
        self.radius_spinbox.setMinimum(0)
        self.radius_spinbox.setMaximum(999999)
        layout.addWidget(QLabel("Dir. reset radius: "))
        layout.addWidget(self.radius_spinbox)
        self.vlayout2.addLayout(layout)

    def add_image_settings(self):
        button1 = QPushButton("Select Tracking Area")
        button1.clicked.connect(MainController.select_tracking_area)
        self.vlayout2.addWidget(button1)

        button2 = QPushButton("Select Color Range")
        button2.clicked.connect(MainController.select_range)
        self.vlayout2.addWidget(button2)

    def add_camera_settings(self):
        textLabel = QLabel('Select webcam')
        cb = QComboBox()
        cb.addItems(["0", "1", "2"])
        cb.setMaximumWidth(100)
        # self.cb.currentIndexChanged.connect(self.selectionchange)
        button = QPushButton('Start capture', self)
        button.setToolTip('This is an example button')
        button.setMaximumWidth(100)
        self.vlayout1.addWidget(textLabel)
        self.vlayout1.addWidget(cb)
        self.vlayout1.addWidget(button)
        self.debug_checkbox = QCheckBox("Show debug")
        self.debug_checkbox.stateChanged.connect(self.toggle_debug)
        self.vlayout1.addWidget(self.debug_checkbox)
        # button.clicked.connect(self.on_click)

    def add_video_thread(self):
        self.thread = VideoThread(MainController)
        self.thread.change_pixmap_signal.connect(self.update_image)
        self.thread.start()
        self.testsignal.emit()

    def add_tabs(self):
        self.tabs = QTabWidget()
        self.tab1 = QWidget()
        self.tab2 = QWidget()
        self.tabs.addTab(self.tab1, "Video Capture")
        self.tabs.addTab(self.tab2, "Gesture Mapping")
        self.mainlayout.addWidget(self.tabs)
        self.mainlayout.setContentsMargins(0, 0, 0, 0)

    def create_layouts_tab1(self):
        gridlayout = QGridLayout()
        vlayout1 = QVBoxLayout()
        vlayout2 = QVBoxLayout()
        vlayout2.setSpacing(0)

        gridlayout.addLayout(vlayout1, 0, 0)
        gridlayout.addLayout(vlayout2, 0, 1)

        gridlayout.setAlignment(Qt.AlignmentFlag.AlignTop)
        vlayout2.setAlignment(Qt.AlignmentFlag.AlignTop)
        vlayout1.setAlignment(Qt.AlignmentFlag.AlignTop)

        return gridlayout, vlayout1, vlayout2

    def create_layouts_tab2(self, tab2):
        hlayout_tab2 = QHBoxLayout()
        gridlayout1_tab2 = QGridLayout()
        gridlayout1_tab2.setAlignment(
            Qt.AlignmentFlag.AlignLeft | Qt.AlignmentFlag.AlignCenter)

        gridlayout2_tab2 = QGridLayout()
        gridlayout2_tab2.setAlignment(
            Qt.AlignmentFlag.AlignLeft | Qt.AlignmentFlag.AlignCenter)

        gridlayout3_tab2 = QGridLayout()
        gridlayout3_tab2.setAlignment(
            Qt.AlignmentFlag.AlignLeft | Qt.AlignmentFlag.AlignCenter)

        tab2.setLayout(hlayout_tab2)
        hlayout_tab2.addLayout(gridlayout1_tab2)
        hlayout_tab2.addLayout(gridlayout2_tab2)
        hlayout_tab2.addLayout(gridlayout3_tab2)

        button = QPushButton("New Sequence")
        gridlayout2_tab2.addWidget(button)
        button.clicked.connect(self.sequence_dialog.show)

        return hlayout_tab2, gridlayout1_tab2, gridlayout2_tab2, gridlayout3_tab2

    def create_keymap_menu(self):
        menu = QMenu(self)

        action = menu.addAction('Keyboard')
        action.triggered.connect(self.keyboardinput_dialog.wait_input)

        action = menu.addAction('Mouse')
        action.triggered.connect(self.mouseinput_dialog.wait_input)

        action = menu.addAction('Hold')
        action.setCheckable(True)
        action.triggered.connect(self.toggle_binding_hold)
        
        action = menu.addAction('Clear')
        action.triggered.connect(self.clear_binding)

        for in_type in CustomBindings.inputs:
            submenu = menu.addMenu(in_type)
            for f in CustomBindings.inputs[in_type]:
                action = submenu.addAction(f.__name__)
                action.triggered.connect(
                    partial(self.update_gesture_binding, in_type, f)
                    )
            menu.addMenu(submenu)

        menu.showEvent = lambda e: self.show_keymap_menu()
        return menu

    def create_sequence_keymap_menu(self):
        menu = QMenu(self)

        action = menu.addAction('Keyboard')
        action.triggered.connect(self.keyboardinput_dialog.wait_input)

        action = menu.addAction('Mouse')
        action.triggered.connect(self.mouseinput_dialog.wait_input)

        action = menu.addAction('Clear')
        action.triggered.connect(self.clear_binding)

        action = menu.addAction('Delete')
        action.triggered.connect(self.delete_sequence)
    
        for in_type in CustomBindings.inputs:
            submenu = menu.addMenu(in_type)
            for f in CustomBindings.inputs[in_type]:
                action = submenu.addAction(f.__name__)
                action.triggered.connect(
                    partial(self.update_gesture_binding, in_type, f)
                    )
            menu.addMenu(submenu)
        
        menu.showEvent = lambda e: self.show_sequence_keymap_menu()
        return menu

    def show_keymap_menu(self):
        for action in self.keymap_menu.actions():
            if action.text() == 'Hold':
                ref = False
                if current_binding_type == "label":
                    ref = MainController.gesture_bindings.bindings[selected_gesture_binding]['hold']
                elif current_binding_type == "direction":
                    ref = MainController.gesture_dir_bindings.bindings[selected_gesture_binding]['hold']
                action.setChecked(ref)

    def delete_sequence(self):
        print(selected_gesture_binding)
        if current_binding_type == "sequence":
            MainController.gesture_sequencer.bindings.pop(
                selected_gesture_binding)
            ref = self.sequence_bindings_ui[selected_gesture_binding]
            self.sequence_bindings_ui.pop(selected_gesture_binding)
            self.gridlayout2_tab2.removeWidget(ref.Qtext)
            self.gridlayout2_tab2.removeWidget(ref.button)
            ref.Qtext.deleteLater()
            ref.button.deleteLater()
            del ref

    def show_sequence_keymap_menu(self):
        pass

    def create_gestlabel_bindings(self):
        line = 0
        for gesture in MainController.gesture_data.available_gestures:
            container = GestureBindingsContainer()
            container.gesture = gesture
            container.Qtext = QLabel(gesture + " : ")
            container.button = QPushButton('+', self)
            container.button.setMaximumWidth(35)
            container.button.setMenu(self.keymap_menu)

            MainController.gesture_bindings.init_bindings()

            self.gestures_bindings_ui[gesture] = container
            self.gridlayout1_tab2.addWidget(container.Qtext, line, 0)
            self.gridlayout1_tab2.addWidget(container.button, line, 1)
            container.update_hover()
            line += 1

    def create_sequence_bindings(self):
        line = 1
        for sequence in MainController.gesture_sequencer.bindings:
            container = GestSeqBindingContainer()
            container.gest_sequence = sequence
            container.show_string = container.get_sequence_string(sequence)
            container.Qtext = QLabel(container.show_string + " : ")
            container.button = QPushButton('+', self)
            container.button.setMaximumWidth(35)
            container.button.setMenu(self.keymap_sequence_menu)

            self.sequence_bindings_ui[sequence] = container
            self.gridlayout2_tab2.addWidget(container.Qtext, line, 0)
            self.gridlayout2_tab2.addWidget(container.button, line, 1)
            container.update_hover()
            line += 1

    def insert_sequence_binding(self, sequence):
        line = list(MainController.gesture_sequencer.bindings.keys()).index(sequence) + 1
        container = GestSeqBindingContainer()
        container.gest_sequence = sequence
        container.show_string = container.get_sequence_string(sequence)
        container.Qtext = QLabel(container.show_string + " : ")
        container.button = QPushButton('+', self)
        container.button.setMaximumWidth(35)
        container.button.setMenu(self.keymap_sequence_menu)
        self.sequence_bindings_ui[sequence] = container
        self.gridlayout2_tab2.addWidget(container.Qtext, line, 0)
        self.gridlayout2_tab2.addWidget(container.button, line, 1)
        container.update_hover()

    def create_direction_bindings(self):
        line = 0
        for bind in MainController.gesture_dir_bindings.bindings:
            container = GestureDirBindingsContainer()
            container.gesture = bind
            container.show_string = container.get_dir_string(bind)
            container.Qtext = QLabel(container.show_string + " : ")
            container.button = QPushButton('+', self)
            container.button.setMaximumWidth(35)
            container.button.setMenu(self.keymap_menu)
            MainController.gesture_dir_bindings.init_bindings()
            self.dir_bindings_ui[bind] = container
            self.gridlayout3_tab2.addWidget(container.Qtext, line, 0)
            self.gridlayout3_tab2.addWidget(container.button, line, 1)
            container.update_hover()
            line += 1

    def create_bindings_menus(self):
        self.create_gestlabel_bindings()
        self.create_sequence_bindings()
        self.create_direction_bindings()

    def create_settings_menu(self):
        self.add_image_settings()
        self.add_sliders()
        self.add_checkboxes_settings()
        self.add_camera_settings()

    def create_tab1_menubar(self):
        menu = QMenuBar()
        filemenu = menu.addMenu("&File")
        action1 = filemenu.addAction("&Open Settings")
        action2 = filemenu.addAction("&Open Color")
        action3 = filemenu.addAction("&Save Settings")
        action4 = filemenu.addAction("&Save Color")
        
        action1.triggered.connect(self.load_settings)
        action2.triggered.connect(self.load_color)
        action3.triggered.connect(self.save_settings)
        action4.triggered.connect(self.save_color)

        self.gridlayout.setMenuBar(menu)

    def create_tab2_menubar(self):
        menu = QMenuBar()
        filemenu = menu.addMenu("&File")
        action1 = filemenu.addAction("&Open Binds")
        action2 = filemenu.addAction("&Save Binds")
        
        action1.triggered.connect(self.load_binds)
        action2.triggered.connect(self.save_binds)

        self.hlayout_tab2.setMenuBar(menu)

    def init_window_settings(self):
        self.setStyleSheet("QLabel, QComboBox, QPushButton, QTabWidget, QCheckBox {font: 12pt Segoe UI}")
        self.setWindowTitle("Gesture Recognition")
        self.width = 700
        self.height = 500
        self.resize(self.width, self.height)
        self.setFixedSize(self.size())

    def toggle_mouse_tracking(self):
        if MainController.TRACKMOUSE:
            MainController.TRACKMOUSE = 0
        else:
            MainController.TRACKMOUSE = 1
            MainController.EXECDIRECTIONBINDS = 0
        self.init_settings()
        # print(MainController.TRACKMOUSE)

    def toggle_gesture_prediction(self):
        MainController.PREDICT = not MainController.PREDICT
        self.init_settings()

    def toggle_sequence(self):
        MainController.SEQUENCE = not MainController.SEQUENCE
        MainController.EXECBINDS = 0
        self.init_settings()

    def toggle_gesture_binds(self):
        MainController.EXECBINDS = not MainController.EXECBINDS
        MainController.SEQUENCE = 0
        self.init_settings()

    def toggle_debug(self):
        MainController.DEBUG = not MainController.DEBUG
        self.init_settings()

    def toggle_direction_binds(self):
        if MainController.EXECDIRECTIONBINDS:
            MainController.EXECDIRECTIONBINDS = 0
        else:
            MainController.EXECDIRECTIONBINDS = 1
            MainController.TRACKMOUSE = 0
        self.init_settings()

    def update_reset_radius(self):
        MainController.set_direction_reset_radius(self.radius_spinbox.value())
        self.init_settings()

    def update_bindings(self):
        for gesture in MainController.gesture_bindings.bindings:
            text = gesture + " : " + get_bind_text(MainController.gesture_bindings.bindings[gesture]["key"])
            self.gestures_bindings_ui[gesture].Qtext.setText(text)

        for gesture in MainController.gesture_sequencer.bindings:
            if gesture not in self.sequence_bindings_ui:
                self.insert_sequence_binding(gesture)
            text = self.sequence_bindings_ui[gesture].show_string + " : " + get_bind_text(MainController.gesture_sequencer.bindings[gesture]["key"])
            self.sequence_bindings_ui[gesture].Qtext.setText(text)
                
        for gesture in MainController.gesture_dir_bindings.bindings:
            text = self.dir_bindings_ui[gesture].show_string + " : " + get_bind_text(MainController.gesture_dir_bindings.bindings[gesture]["key"])
            self.dir_bindings_ui[gesture].Qtext.setText(text)

    def init_settings(self):
        self.direction_checkbox.blockSignals(True)
        self.direction_checkbox.setChecked(MainController.EXECDIRECTIONBINDS)
        self.direction_checkbox.blockSignals(False)
        self.debug_checkbox.blockSignals(True)
        self.debug_checkbox.setChecked(MainController.DEBUG)
        self.debug_checkbox.blockSignals(False)
        self.tracking_checkbox.blockSignals(True)
        self.tracking_checkbox.setChecked(MainController.TRACKMOUSE)
        self.tracking_checkbox.blockSignals(False)
        self.sequence_checkbox.blockSignals(True)
        self.sequence_checkbox.setChecked(MainController.SEQUENCE)
        self.sequence_checkbox.blockSignals(False)
        self.binds_checkbox.blockSignals(True)
        self.binds_checkbox.setChecked(MainController.EXECBINDS)
        self.binds_checkbox.blockSignals(False)
        self.prediction_checkbox.blockSignals(True)
        self.prediction_checkbox.setChecked(MainController.PREDICT)
        self.prediction_checkbox.blockSignals(False)
        self.hand_checkbox.blockSignals(True)
        self.hand_checkbox.setChecked(MainController.flip_hand)
        self.hand_checkbox.blockSignals(False)
        self.radius_spinbox.setValue(
            MainController.gesture_data.direction_reset_size)
        self.update_slider_values()

    def insert_binding_functions(self):
        CustomBindings.inputs["Functions"].extend([
            self.toggle_direction_binds, 
            self.toggle_gesture_binds, 
            self.toggle_sequence,
            self.toggle_gesture_prediction,
            self.toggle_mouse_tracking,
            self.toggle_debug])

    def post_init(self):
        self.gridlayout1_tab2.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.gridlayout2_tab2.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.gridlayout3_tab2.setAlignment(Qt.AlignmentFlag.AlignCenter)

        Serializer.load_settings('config/settings_default.json')
        Serializer.load_binds('binds.py')
        self.update_bindings()
        self.init_settings()

    def __init__(self):
        super().__init__()
        Serializer.gui = self
        self.insert_binding_functions()
        self.init_window_settings()
        self.keyboardinput_dialog = KeyboardInputDialog(self)
        self.mouseinput_dialog = MouseInputDialog(self)
        self.sequence_dialog = SequenceCreatorDialog(self)

        self.mainlayout = QVBoxLayout()
        self.setLayout(self.mainlayout)

        self.add_tabs()
        self.cv_image = QLabel(self)
        self.keymap_menu = self.create_keymap_menu()
        self.keymap_sequence_menu = self.create_sequence_keymap_menu()

        self.gridlayout, self.vlayout1, self.vlayout2 = self.create_layouts_tab1()
        self.tab1.setLayout(self.gridlayout)

        self.hlayout_tab2, self.gridlayout1_tab2, self.gridlayout2_tab2, self.gridlayout3_tab2 = self.create_layouts_tab2(
            self.tab2)

        self.add_video_thread()
        self.vlayout1.addWidget(self.cv_image)

        self.sliders = self.create_sliders()
        self.tracking_checkbox = None
        self.prediction_checkbox = None
        self.direction_checkbox = None
        self.sequence_checkbox = None
        self.binds_checkbox = None
        self.debug_checkbox = None
        self.radius_spinbox = None
        self.hand_checkbox = None

        self.create_bindings_menus()
        self.create_settings_menu()

        self.setFocus()
        app.focusChanged.connect(self.on_focusChanged)
        self.init_settings()

        self.create_tab1_menubar()
        self.create_tab2_menubar()
        
        self.post_init()

    def closeEvent(self, event):
        self.thread.stop()
        event.accept()

    def on_focusChanged(self):
        self.update_slider_values()

    def keyPressEvent(self, event):
        if event.key() == Qt.Key.Key_F1:
            self.tracking_checkbox.setChecked(not bool(MainController.TRACKMOUSE))
        if event.key() == Qt.Key.Key_F2:
            Serializer.save_binds('config/binds.py')
        if event.key() == Qt.Key.Key_F3:
            Serializer.load_binds('binds.py')
            self.update_bindings()

    def save_binds(self):
        try:
            Serializer.save_binds(self.saveFileDialog("Python files", "py"))
        except Exception as e:
            msg = QErrorMessage()
            msg.showMessage(str(e))
            msg.exec_()

    def load_binds(self):
        try:
            Serializer.load_binds(os.path.basename(self.openFileNameDialog("Python files", "py")))
            self.update_bindings()
        except Exception as e:
            msg = QErrorMessage()
            msg.showMessage(str(e))
            msg.exec_()

    def save_settings(self):
        try:
            Serializer.save_settings_to_file(self.saveFileDialog())
        except Exception as e:
            msg = QErrorMessage()
            msg.showMessage(str(e))
            msg.exec_()

    def load_settings(self):
        try:
            Serializer.load_settings(self.openFileNameDialog())
            self.init_settings()
        except Exception as e:
            msg = QErrorMessage()
            msg.showMessage(str(e))
            msg.exec_()
    
    def save_color(self):
        try:
            Serializer.save_color_to_file(self.saveFileDialog())
        except Exception as e:
            msg = QErrorMessage()
            msg.showMessage(str(e))
            msg.exec_()

    def load_color(self):
        try:
            Serializer.load_color(self.openFileNameDialog())
            self.init_settings()
        except Exception as e:
            msg = QErrorMessage()
            msg.showMessage(str(e))
            msg.exec_()

    # https://pythonspot.com/pyqt5-file-dialog/
    def openFileNameDialog(self, filetypename = "JSON Files" , filetypeext = "json"):
        options = QFileDialog.Options()
        fileName, _ = QFileDialog.getOpenFileName(self,"Open file", "", filetypename + " (*." + filetypeext + ");;All Files (*)", options=options)
        if fileName:
            return fileName
        else:
            raise Exception("Could not open file name")
    
    def saveFileDialog(self, filetypename = "JSON Files" , filetypeext = "json"):
        options = QFileDialog.Options()
        fileName, _ = QFileDialog.getSaveFileName(self,"Open file", "", filetypename + " (*." + filetypeext + ");;All Files (*)", options=options)
        if fileName:
            return fileName
        else:
            raise Exception("Could not save file name")
    
    # https://gist.github.com/docPhil99/ca4da12c9d6f29b9cea137b617c7b8b1
    @pyqtSlot(np.ndarray)
    def update_image(self, cv_img):
        """Updates the cv_image with a new opencv image"""
        qt_img = self.convert_cv_qt(cv_img)
        self.cv_image.setPixmap(qt_img)
    
    def convert_cv_qt(self, cv_img):
        """Convert from an opencv image to QPixmap"""
        rgb_image = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
        h, w, ch = rgb_image.shape
        bytes_per_line = ch * w
        convert_to_Qt_format = QtGui.QImage(rgb_image.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
        p = convert_to_Qt_format.scaled(512, 300, Qt.KeepAspectRatio)
        return QPixmap.fromImage(p)


if __name__=="__main__":
    app = QApplication(sys.argv)
    mainapp = App()

    mainapp.show()
    sys.exit(app.exec_())
