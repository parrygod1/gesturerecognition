from pynput.mouse import Controller as MouseController, Button
from pynput.keyboard import Key, Controller, KeyCode
import pynput
import time

def current_milli_time():
        return round(time.time() * 1000)

def testfunc():
    print('sequence match!')
    #exit(0)

class InputController:
    keyboard = Controller()
    mouse = MouseController()

class CustomBindings:
        inputs = {
            "Functions" : []
        }


class BindingTemplate:
    def __init__(self):
        self.bindings = {}
    
    def init_bindings(self):
        for label in self.bindings:
            self.bindings[label] = {'key' : None, 'hold': False}
    
    def add_binding(self, bind, key):
        self.bindings[gesture] = key
        print(self.bindings)

    def reset_press(self):
        for gesture in self.bindings:
            if self.bindings[gesture]['key'] != None and not callable(self.bindings[gesture]['key']):
                keytype = self.get_keytype(self.bindings[gesture]['key'])
                keytype.release(self.bindings[gesture]['key'])

    def get_keytype(self, binding):
        return InputController.mouse if isinstance(binding, pynput.mouse.Button) else InputController.keyboard

    def exec_bind(self, gesture):
        self.reset_press()
        if gesture in self.bindings and self.bindings[gesture] != None\
        and self.bindings[gesture]['key'] != None:
            if callable(self.bindings[gesture]['key']):
                self.bindings[gesture]['key']()
            else:
                keytype = self.get_keytype(self.bindings[gesture]['key'])
                if self.bindings[gesture]['hold']:
                    keytype.press(self.bindings[gesture]['key'])
                else:
                    keytype.press(self.bindings[gesture]['key'])
                    keytype.release(self.bindings[gesture]['key'])
    

class GestureInputBindings(BindingTemplate): 
    def __init__(self):
        self.bindings = {
            "L" : {
                'key' : 'a',
                'hold' : True
            }, 
            "2" : {'key': Button.left, 'hold': False},
            "C" : {},
            "3" : {},
            "4" : {},
            "palm": {},
        }
        self.init_bindings()

class GestureDirections(BindingTemplate):
    STATIC = 0
    UP = 1
    DOWN = 2
    LEFT = 3
    RIGHT = 4
    named_mapping = {
        "up" : UP,
        "down" : DOWN,
        "left": LEFT,
        "right": RIGHT,
        "static" : STATIC
    }
    named_mapping_inv = {v: k for k, v in named_mapping.items()}

    def __init__(self):
        self.bindings = {
            ("left", "static") : "",
            ("right", "static") : "",
            ("static", "up") : "",
            ("static", "down") : "",
            ("left", "up") : "",
            ("right", "up") : "",
            ("left", "down") : "",
            ("right", "down") : "",
        }
        self.init_bindings()
        
    def exec_bind(self, gesture):
        x = self.named_mapping_inv[gesture[0]]
        y = self.named_mapping_inv[gesture[1]]
        gesture = (x,y)
        super().exec_bind(gesture)
    
    def reset_press(self):
        super().reset_press()


class GestureSequencer(BindingTemplate):
    time_window = 3000 #time in ms to wait for a new gesture
    start_time = 0
    current_time = 0
    min_gestures = 2 #sequence should look like (neutral, gest1, gest2, neutral)


    def __init__(self):
        self.current_sequence = []
        self.bindings = {
            ("palm", "L", "C", "palm") : {},
        } #functions to call on each sequence
        self.neutral_gesture = ""
        self.init_bindings()

    def match_current_sequence(self):
        seq = tuple(self.current_sequence)
        if seq in self.bindings:
            if callable(self.bindings[seq]['key']):
                self.bindings[seq]['key']()
                self.current_sequence.clear()
                return True
            elif self.bindings[seq] != None and self.bindings[seq]['key'] != None:
                keytype = self.get_keytype(self.bindings[seq]['key'])
                keytype.press(self.bindings[seq]['key'])
                keytype.release(self.bindings[seq]['key'])
                return True
        return False

    def reset_sequence(self):
        last = self.current_sequence[-1] if len(self.current_sequence) > 0 else None
        self.current_sequence.clear()
        self.reset_sequencer_time()
        if last is not None:
            self.current_sequence.append(last)

    def check_sequence(self):
        print(self.current_sequence)
        print(self.match_current_sequence())
        self.reset_sequence()
        
    def add_gest_sequence(self, gest_label):
        self.current_sequence.append(gest_label)
        if len(self.current_sequence) >= self.min_gestures and\
                gest_label == self.neutral_gesture:
            self.check_sequence()
        self.reset_sequencer_time()
    
    def insert_full_sequence(self, sequence):
        sequence = [label for label in sequence if label != self.neutral_gesture]
        if len(sequence) < self.min_gestures:
            raise Exception("Minimum sequence length is " + str(self.min_gestures))
        sequence.insert(0, self.neutral_gesture)
        sequence.append(self.neutral_gesture)
        if tuple(sequence) not in self.bindings:
            self.bindings[tuple(sequence)] = {'key' : None, 'hold': False}
        else:
            raise Exception("Sequence already exsits!")

        return tuple(sequence)

    def reset_sequencer_time(self):
        self.current_time = current_milli_time()
        self.start_time = current_milli_time()

    def update_sequencer_time(self):
        #print(self.current_sequence)
        self.current_time = current_milli_time()
        if self.current_time - self.start_time >= self.time_window:
            self.reset_sequence()