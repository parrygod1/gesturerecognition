#import gestrecog
import json
import pynput
import re
from ctypes import windll
from importlib import import_module

class GestureRecognitionSearlize:
    def __init__(self, controller_obj):
        self.controller = controller_obj
        self.gui = None
        self.config_folder = 'config/'

    def settings_to_json(self):
        data = {
            "PREDICT": self.controller.PREDICT,
            "SEQUENCE": self.controller.SEQUENCE,
            "EXECBINDS": self.controller.EXECBINDS,
            "EXECDIRECTIONBINDS": self.controller.EXECDIRECTIONBINDS,
            "TRACKMOUSE": self.controller.TRACKMOUSE,
            "DEBUG": self.controller.DEBUG,
            "dir_reset_radius": self.controller.gesture_data.direction_reset_size,
            "left_hand" : self.controller.flip_hand,
            "tracking_area": {
                "p1": [self.controller.gesture_data.tracking_area[0][0], self.controller.gesture_data.tracking_area[0][1]],
                "p2": [self.controller.gesture_data.tracking_area[1][0], self.controller.gesture_data.tracking_area[1][1]],
            }
        }
        return data

    def color_to_json(self):
        data = {
            "range_lower": self.controller.range1,
            "range_upper": self.controller.range2,
        }
        return data

    def save_data(self, filename, data):
        try:
            f = open(filename, 'x')
            f.close()
        except Exception as e:
            print(e)

        with open(filename, 'w') as outfile:
            json.dump(data, outfile)

    def save_settings_to_file(self, filename):
        self.save_data(filename, self.settings_to_json())

    def save_color_to_file(self, filename):
        self.save_data(filename, self.color_to_json())

    def tuple_bind_to_str(self, bind):
        if isinstance(bind, tuple):
            newstr = ""
            for label in bind:
                newstr += label + ","
            return newstr
        return bind
    
    def bindings_to_pyfile(self, dict_bindings, name):
        retstr = f'{name} : '
        data = {}
        for bind in dict_bindings:
            data[bind] = dict_bindings[bind]
            key = dict_bindings[bind]["key"]
            
            if isinstance(key, pynput.mouse.Button):
                key = key.__module__.replace("_win32", "Button." + key.name)
            elif isinstance(key, pynput.keyboard.Key):
                key = key.__module__.replace("_win32", "Key." + key.name)
            elif callable(key):
                key = "gui." + key.__name__
            data[bind]["key"] = key 
        retstr += str(data)

        pattern1 = "('(pynput.mouse.Button.[a-z]+)')"
        pattern2 = "'(gui.([a-z]+_[a-z]+)+)'"
        pattern3 = "'(pynput.keyboard.Key.[a-z]+(\_?[a-z]+)+)'"
        replace_group1 = 2
        replace_group2 = 1
        replace_group3 = 1

        match = re.search(pattern1, retstr)
        while match is not None:
            retstr = retstr.replace(match.group(0), match.group(replace_group1))
            match = re.search(pattern1, retstr)
        
        match = re.search(pattern2, retstr)
        while match is not None:
            retstr = retstr.replace(match.group(0), match.group(replace_group2))
            match = re.search(pattern2, retstr)
        
        match = re.search(pattern3, retstr)
        while match is not None:
            retstr = retstr.replace(match.group(0), match.group(replace_group3))
            match = re.search(pattern3, retstr)
        #print(retstr)
        return retstr

    def save_binds(self, filename):
        to_save = "import pynput\ngui=None\ndef get_binds():\n\treturn {\n"
        to_save += "\t\t" + self.bindings_to_pyfile(self.controller.gesture_bindings.bindings, "'gesture_bindings'") + ",\n"
        to_save += "\t\t" + self.bindings_to_pyfile(self.controller.gesture_sequencer.bindings, "'sequence_bindings'") + ",\n"
        to_save += "\t\t" + self.bindings_to_pyfile(self.controller.gesture_dir_bindings.bindings, "'direction_bindings'") + "\n"
        to_save += "\t}"
        with open(filename, 'w') as outfile:
            outfile.write(to_save)

    def load_binds(self, filename):
        imp = import_module("config." + filename.strip('.py'))
        setattr(imp, "gui", self.gui)
        self.controller.gesture_bindings.bindings = getattr(imp, "get_binds")()['gesture_bindings']
        self.controller.gesture_sequencer.bindings = getattr(imp, "get_binds")()["sequence_bindings"]
        self.controller.gesture_dir_bindings.bindings = getattr(imp, "get_binds")()["direction_bindings"]
        
    def load_settings(self, filename):
        with open(filename, 'r') as infile:
            data = json.load(infile)
            self.controller.PREDICT = data["PREDICT"]
            self.controller.SEQUENCE = data["SEQUENCE"]
            self.controller.EXECBINDS = data["EXECBINDS"]
            self.controller.EXECDIRECTIONBINDS = data["EXECDIRECTIONBINDS"]
            self.controller.TRACKMOUSE = data["TRACKMOUSE"]
            self.controller.DEBUG = data["DEBUG"]
            self.controller.flip_hand = data["left_hand"]
            self.controller.gesture_data.direction_reset_size = data["dir_reset_radius"]

    def load_color(self, filename):
        with open(filename, 'r') as infile:
            data = json.load(infile)
            self.controller.range1 = data["range_lower"]
            self.controller.range2 = data["range_upper"]
            print(self.controller.range1, self.controller.range2)
        
