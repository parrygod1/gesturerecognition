#Python ver 3.8.7 64bit
from cv2 import cv2
import model
import imseg
import numpy as np
import time
import gestserialize
from gestinputs import GestureDirections, GestureInputBindings, GestureSequencer, InputController

class Utils:
    pred_text = ""

    #https://stackoverflow.com/a/5998359/2630434
    def current_milli_time():
        return round(time.time() * 1000)

    #https://forum.unity.com/threads/re-map-a-number-from-one-range-to-another.119437/#post-800377
    def map_range(value, lower1, upper1, lower2, upper2):
        return (value - lower1) / (upper1 - lower1) * (upper2 - lower2) + lower2

    def normalize_pos(shape, x, y, lower1 = 0, lower2 = 0):
        n_x = Utils.map_range(x, lower1, shape[0], 0, 1920)
        n_y = Utils.map_range(y, lower2, shape[1], 0, 1080)
        #print(x,y, "->", n_x, n_y)
        return (n_x, n_y)

    def show_box_center(frame, center):
        cv2.circle(frame, (center[0],center[1]), 5, (0,0,255))

    def show_fps(frame, timer):
        fps = 1.0 / (time.time() - timer)
        cv2.putText(frame, str(int(fps)), (30,70), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,), 2)

    def show_gesture(frame, pred_text):
        cv2.putText(frame, pred_text, (30,30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,255), 2)

    def show_dir_reset(frame, circle_size):
        cv2.circle(frame, (int(frame.shape[1]/2), int(frame.shape[0]/2)), circle_size, (0,0,255), 2)

    def show_tracking_area(frame, rectangle):
        cv2.rectangle(frame,rectangle[0],rectangle[1],(0,255,0),2)

    def get_img_mask(frame, range1, range2, color_mode):
        mask = []
        if color_mode == ColorMode.HSV:
            mask = imseg.seg_color_range(frame, np.array(range1), np.array(range2))
        elif color_mode == ColorMode.YCBCR:
            mask = imseg.seg_color_range(frame, np.array(range1), np.array(range2))
        else:
            raise Exception("Invalid image segmentation color mode")

        area, img, center = imseg.get_largest_contour(mask, frame)
        return area, img, center

class GestureData:
    direction_radius_deadzone = 40 #amount of radius px to consider position static
    direction_start_time = 0
    direction_current_time = 0
    direction_check_delay = 100
    direction_reset_size = 100

    start_time = 0
    current_time = 0
    check_delay = 500

    predict_gesture_callback = None #will be called when a new gesture is recognized
    new_dir_callback = None
    dir_reset_callback = None

    def __init__(self):
        self.prev_pos = (0, 0)
        self.current_pos = (0, 0)
        self.current_gest = None
        self.prev_dir_x = GestureDirections.STATIC
        self.prev_dir_y = GestureDirections.STATIC
        self.current_dir_x = GestureDirections.STATIC
        self.current_dir_y = GestureDirections.STATIC
        self.available_gestures = self.pool_available_gestures()
        self.neutral_gesture = "palm"
        self.can_exec_dir_bind = True
        self.invert_x_tracking = True
        self.tracking_area = ((0,0), (0,0))

    def pool_available_gestures(self):
        l = []
        for label in model.classes:
            l.append(label)
        return l

    def check_diff_prev_dirs(self):
        if self.prev_dir_x != self.current_dir_x or self.prev_dir_y != self.current_dir_y:
            #print(GestureDirections.named_mapping_inv[self.current_dir_x], GestureDirections.named_mapping_inv[self.current_dir_y])
            #print(GestureDirections.named_mapping_inv[self.current_dir_x], GestureDirections.named_mapping_inv[self.current_dir_y])
            return True
        return False

    def update_prev_dirs(self):
        self.prev_dir_x = self.current_dir_x
        self.prev_dir_y = self.current_dir_y
        self.prev_pos = self.current_pos
    
    def calc_direction(self):
        if abs(self.current_pos[0] - self.prev_pos[0]) >= self.direction_radius_deadzone:
            if self.current_pos[0] < self.prev_pos[0]:
                self.current_dir_x = GestureDirections.LEFT
            elif self.current_pos[0] > self.prev_pos[0]:
                self.current_dir_x = GestureDirections.RIGHT 
        else:
            self.current_dir_x = GestureDirections.STATIC

        if abs(self.current_pos[1] - self.prev_pos[1]) >= self.direction_radius_deadzone:
            if self.current_pos[1] < self.prev_pos[1]:
                self.current_dir_y = GestureDirections.UP
            elif self.current_pos[1] > self.prev_pos[1]:
                self.current_dir_y = GestureDirections.DOWN
        else: 
            self.current_dir_y = GestureDirections.STATIC
    
    def check_direction_reset(self, frame):
        if frame.shape > (100, 100):
            center = (int(frame.shape[1] / 2), int(frame.shape[0]/2))
            if cv2.norm(self.current_pos, center, cv2.NORM_L2) <= self.direction_reset_size:
                self.can_exec_dir_bind = True
                if callable(self.dir_reset_callback):
                    self.dir_reset_callback()
        #print(self.can_exec_dir_bind)

    def is_outside_reset(self, frame):
        if frame.shape > (100, 100):
            center = (int(frame.shape[1] / 2), int(frame.shape[0]/2))
            if cv2.norm(self.current_pos, center, cv2.NORM_L2) > self.direction_reset_size:
                return True
        return False

    def check_direction(self, frame):  
        if self.direction_current_time - self.direction_start_time >= self.direction_check_delay:
            self.check_direction_reset(frame)
            self.calc_direction()
            if self.check_diff_prev_dirs() and callable(self.new_dir_callback):
                self.new_dir_callback()
                self.can_exec_dir_bind = False
            self.update_prev_dirs()

            self.direction_start_time = Utils.current_milli_time()
            self.direction_current_time = Utils.current_milli_time()

    def update_direction_time(self, pos):
        self.direction_current_time = Utils.current_milli_time()
        self.current_pos = pos

    def predict_gesture(self, img):
        label = self.current_gest
        if self.current_time - self.start_time >= self.check_delay\
                and img is not None:
            self.start_time = Utils.current_milli_time()
            self.current_time = Utils.current_milli_time()
            if img.shape[0] > 0 and img.shape[1] > 0:
                new_label = model.predict_gesture(img)
                if new_label != 'none' and new_label != label:
                    label = new_label
                    if callable(self.predict_gesture_callback):
                        self.predict_gesture_callback(label)
            self.current_gest = label
        return label

    def update_gesture_time(self):
         self.current_time = Utils.current_milli_time()

    def in_tracking_area(self, pos):
        area = self.tracking_area
        if area[0][0] < pos[0] and pos[0] < area[1][0]\
        and area[0][1] < pos[1] and pos[1] < area[1][1]:
            return True
        return False

    def get_tracking_area_size(self):
        area = self.tracking_area
        return (abs(area[1][0]- area[0][0]), abs(area[1][1] - area[0][1]))

class ColorMode:
    RGB = 0
    BGR = 1
    HSV = 2
    YCBCR = 3

class GestureRecognitionController:
    PREDICT = 1
    SEQUENCE = 0
    EXECBINDS = 0
    EXECDIRECTIONBINDS = 0
    TRACKMOUSE = 0
    DEBUG = 1

    def __init__(self):
        self.cap = None
        self.gesture_bindings = GestureInputBindings()
        self.gesture_dir_bindings = GestureDirections()
        self.gesture_data = GestureData()
        self.gesture_sequencer = GestureSequencer()
        self.gesture_sequencer.neutral_gesture = self.gesture_data.neutral_gesture
        self.gesture_data.predict_gesture_callback = self.handle_new_gesture
        self.gesture_data.new_dir_callback = self.handle_new_dir
        self.gesture_data.dir_reset_callback = self.gesture_dir_bindings.reset_press

        self.frame = []
        self.flip_hand = False
        self.color_mode = ColorMode.HSV
        self.range1 = [81, 96, 60]
        self.range2 = [104, 255, 255]
        self.tracking_area_offset = 50
        imseg.DEBUG = self.DEBUG

    def handle_new_gesture(self, gesture):
        if self.PREDICT:
            if self.SEQUENCE:
                self.gesture_sequencer.add_gest_sequence(gesture)

            if self.EXECBINDS:
                self.gesture_bindings.reset_press()
                self.gesture_bindings.exec_bind(gesture)

    def handle_new_dir(self):
        if self.EXECDIRECTIONBINDS and self.gesture_data.can_exec_dir_bind\
            and self.gesture_data.is_outside_reset(self.frame):
            self.gesture_dir_bindings.exec_bind(\
                (self.gesture_data.current_dir_x,\
                self.gesture_data.current_dir_y))

    def set_tracking_area(self, frame):
        self.gesture_data.tracking_area = frame
            
    def select_tracking_area(self):
        ret, frame = self.cap.read()
        setting = self.TRACKMOUSE
        self.TRACKMOUSE = 0
        r = cv2.selectROI('Select area', frame)
        self.set_tracking_area(((r[0], r[1]), (r[0]+r[2], r[1]+r[3])))
        cv2.destroyWindow('Select area')
        self.TRACKMOUSE = setting

    def init_tracking_area(self):
        ret, frame = self.cap.read()
        off = self.tracking_area_offset
        self.set_tracking_area(((0 + off, 0 + off), (frame.shape[1] - off, frame.shape[0] - off)))

    #https://stackoverflow.com/questions/50851658/opencv-selectroi-returning-zero-tuple
    def select_range(self):
        ret, frame = self.cap.read()
        r = cv2.selectROI('Select color', frame)
        if r[0] > 5 and r[1] > 5:
            imCrop = frame[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+r[2])]
            imCrop = cv2.cvtColor(imCrop, cv2.COLOR_BGR2HSV)
            h_range = (imCrop[..., 0].min(), imCrop[..., 0].max())
            s_range = (imCrop[..., 1].min(), imCrop[..., 1].max())
            v_range = (imCrop[..., 2].min(), imCrop[..., 2].max())
            self.range1 = np.array((h_range[0], s_range[0], v_range[0]))
            self.range2 = np.array((h_range[1], s_range[1], v_range[1]))
        cv2.destroyWindow('Select color')

    def update_mouse_pos(self, center):
        if self.gesture_data.in_tracking_area(center):
            shape = self.gesture_data.get_tracking_area_size()
            area = self.gesture_data.tracking_area
            shape = (area[0][0] + shape[0], area[0][1] + shape[1])
            InputController.mouse.position = Utils.normalize_pos(shape, center[0], center[1], area[0][0], area[0][1])

    def process(self, timer):
        area, img, center = Utils.get_img_mask(self.frame, self.range1, self.range2, self.color_mode)
        
        if self.flip_hand:
            area = cv2.flip(area, 1)

        if self.PREDICT:
            self.gesture_data.update_gesture_time()
            Utils.pred_text = self.gesture_data.predict_gesture(area)

        if self.SEQUENCE:
            self.gesture_sequencer.update_sequencer_time()
        
        if self.TRACKMOUSE:
            self.update_mouse_pos(center)

        self.gesture_data.update_direction_time(center)
        self.gesture_data.check_direction(self.frame)

        if self.DEBUG:
            Utils.show_gesture(self.frame, Utils.pred_text)
            Utils.show_box_center(self.frame, center)
            Utils.show_dir_reset(self.frame, self.gesture_data.direction_reset_size)
            Utils.show_tracking_area(self.frame, self.gesture_data.tracking_area)
            #if area is not None:
            #    cv2.imshow('mask', area)
        
    def change_hand(self):
        self.flip_hand = not self.flip_hand

    def update_range_h(self, v1, v2):
        self.range1[0] = v1
        self.range2[0] = v2
    
    def update_range_s(self, v1, v2):
        self.range1[1] = v1
        self.range2[1] = v2
    
    def update_range_v(self, v1, v2):
        self.range1[2] = v1
        self.range2[2] = v2

    def update_frame(self, framen):
        self.frame = framen
        
    def get_frame(self):
        return self.frame
    
    def set_debug(self, b):
        self.DEBUG = b
        imseg.DEBUG = self.DEBUG
    
    def set_direction_reset_radius(self, radius):
        self.gesture_data.direction_reset_size = int(radius)
        print(radius)

cap = cv2.VideoCapture('videos/modeltest.mp4')
#cap = cv2.VideoCapture(0)
model.init('model/6gesturemodel.hdf5')

if __name__ == "__main__":
    MainController = GestureRecognitionController()
    #MainController.range1 = (cv2.getTrackbarPos('H1','image'), cv2.getTrackbarPos('S1','image'), cv2.getTrackbarPos('V1','image'))
    #MainController.range2 = (cv2.getTrackbarPos('H2','image'), cv2.getTrackbarPos('S2','image'), cv2.getTrackbarPos('V2','image'))

    if cap.isOpened():
        MainController.cap = cap
        MainController.init_tracking_area()

    while cap.isOpened():
        timer = time.time() 
        ret, frame = cap.read()
        
        if(cv2.waitKey(10) & 0xFF == ord('h')):
            imseg.seg_color_range_test(frame, np.array(MainController.range1), np.array(MainController.range2))
        
        MainController.update_frame(frame)
        MainController.process(timer)
        new_frame = MainController.get_frame()

        cv2.imshow('video', new_frame)
    
        if(cv2.waitKey(10) & 0xFF == ord('q')):
            quit(0)
            break
        elif(cv2.waitKey(10) & 0xFF == ord('g')):
            MainController.select_tracking_area()
        elif(cv2.waitKey(10) & 0xFF == ord('f')):
            MainController.select_range()

    cap.release()
    cv2.destroyAllWindows()

