import pynput
gui=None
def get_binds():
	return {
		'gesture_bindings' : {'L': {'key': None, 'hold': True}, '2': {'key': None, 'hold': False}, 'C': {'key': None, 'hold': False}, '3': {'key': None, 'hold': False}, '4': {'key': None, 'hold': False}, 'palm': {'key': None, 'hold': False}},
		'sequence_bindings' : {('palm', 'C', '2', 'palm'): {'key': pynput.keyboard.Key.space, 'hold': False}},
		'direction_bindings' : {('left', 'static'): {'key': pynput.keyboard.Key.left, 'hold': False}, ('right', 'static'): {'key': pynput.keyboard.Key.right, 'hold': False}, ('static', 'up'): {'key': pynput.keyboard.Key.up, 'hold': False}, ('static', 'down'): {'key': pynput.keyboard.Key.down, 'hold': False}, ('left', 'up'): {'key': None, 'hold': False}, ('right', 'up'): {'key': None, 'hold': False}, ('left', 'down'): {'key': None, 'hold': False}, ('right', 'down'): {'key': None, 'hold': False}}
	}