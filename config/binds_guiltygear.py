import pynput
gui=None
def get_binds():
	return {
		'gesture_bindings' : {'L': {'key': 'k', 'hold': True}, '2': {'key': 'l', 'hold': True}, 'C': {'key': None, 'hold': False}, '3': {'key': None, 'hold': False}, '4': {'key': None, 'hold': False}, 'palm': {'key': None, 'hold': False}},
		'sequence_bindings' : {('palm', 'L', 'C', 'palm'): {'key': 'a', 'hold': False}},
		'direction_bindings' : {('left', 'static'): {'key': 'a', 'hold': True}, ('right', 'static'): {'key': 'd', 'hold': True}, ('static', 'up'): {'key': 'w', 'hold': True}, ('static', 'down'): {'key': 's', 'hold': True}, ('left', 'up'): {'key': None, 'hold': False}, ('right', 'up'): {'key': None, 'hold': False}, ('left', 'down'): {'key': None, 'hold': False}, ('right', 'down'): {'key': None, 'hold': False}}
	}