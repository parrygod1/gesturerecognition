import pynput
gui=None
def get_binds():
	return {
		'gesture_bindings' : {'L': {'key': pynput.mouse.Button.left, 'hold': True}, '2': {'key': gui.toggle_debug, 'hold': False}, 'C': {'key': None, 'hold': False}, '3': {'key': None, 'hold': False}, '4': {'key': None, 'hold': False}, 'palm': {'key': None, 'hold': False}},
		'sequence_bindings' : {('palm', 'L', 'C', 'palm'): {'key': pynput.mouse.Button.left, 'hold': False}, ('palm', '2', '4', 'palm'): {'key': pynput.keyboard.Key.space, 'hold': False}},
		'direction_bindings' : {('left', 'static'): {'key': 'a', 'hold': False}, ('right', 'static'): {'key': 'd', 'hold': False}, ('static', 'up'): {'key': 'w', 'hold': False}, ('static', 'down'): {'key': 's', 'hold': False}, ('left', 'up'): {'key': None, 'hold': False}, ('right', 'up'): {'key': None, 'hold': False}, ('left', 'down'): {'key': None, 'hold': False}, ('right', 'down'): {'key': None, 'hold': False}}
	}