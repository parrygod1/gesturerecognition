import pynput
gui=None
def get_binds():
	return {
		'gesture_bindings' : {'L': {'key': pynput.mouse.Button.left, 'hold': False}, '2': {'key': None, 'hold': False}, 'C': {'key': None, 'hold': False}, '3': {'key': gui.toggle_sequence, 'hold': False}, '4': {'key': gui.toggle_mouse_tracking, 'hold': False}, 'palm': {'key': None, 'hold': False}},
		'sequence_bindings' : {('palm', 'L', 'C', 'palm'): {'key': 'pynput.keyboard.Key.f11', 'hold': False}},
		'direction_bindings' : {('left', 'static'): {'key': None, 'hold': False}, ('right', 'static'): {'key': None, 'hold': False}, ('static', 'up'): {'key': pynput.keyboard.Key.page_up, 'hold': False}, ('static', 'down'): {'key': pynput.keyboard.Key.page_down, 'hold': False}, ('left', 'up'): {'key': None, 'hold': False}, ('right', 'up'): {'key': None, 'hold': False}, ('left', 'down'): {'key': None, 'hold': False}, ('right', 'down'): {'key': None, 'hold': False}}
	}